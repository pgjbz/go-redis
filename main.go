package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"time"

	"pgjbz.dev/go-redis/database"
	"pgjbz.dev/go-redis/parser"
	"pgjbz.dev/go-redis/util"
)

func sendPing() {
	con, err := net.Dial("tcp", "0.0.0.0:8080")
	if err != nil {
		log.Fatal(err)
	}

	for {
		readerBuffer := make([]byte, 1024)
		con.Write(getStringBytes("*1\r\n$3\r\nset\r\n$4\r\nmila\r\n$4\r\nlove\r\n$2px\r\n$3\r\n100"))
		con.Read(readerBuffer)
		log.Println(string(readerBuffer))
		time.Sleep(1 * time.Second)
	}

}

var db database.InMemory

func init() {
	db = database.NewInMemory()
}

func main() {
	con, err := net.Listen("tcp", ":8080")
	writeIfError(err, nil)
	go sendPing()
	go func() {
		for {
			time.Sleep(time.Millisecond * 25)
			db.Clear()
		}
	}()
	for {
		req, err := con.Accept()
		log.Println("receive request...")
		go handleConnection(req, err)
	}
}

func handleConnection(con net.Conn, err error) {
	checkError(err)
	defer func() {
		log.Println("closing connection...")
		con.Close()
	}()
	for {
		buffer := make([]byte, 4096)

		_, err := con.Read(buffer)
		if err == io.EOF {
			log.Print("connection closed")
			break
		}
		writeIfError(err, con)

		log.Println("parsing request")
		parsed, err := parser.Parse(buffer)

		writeIfError(err, con)
		for _, cmd := range parsed {
			fmt.Printf("%v", cmd)
			switch cmd.Cmd {
			case "ping":
				con.Write(getStringBytes("+pong\r\n"))
			case "set":
				data := database.NewData(cmd.Value)

				for key, val := range cmd.Args {
					if key == "px" {
						data.Expire = true
						parsed := util.ParseInt(val)
						now := time.Now()
						now.Add(time.Millisecond * time.Duration(parsed))
					}
				}

				db.Insert(cmd.Key, data)
				con.Write(getStringBytes("+ok\r\n"))
			case "get":
				val, err := db.Get(cmd.Key)
				if err != nil {
					con.Write(getStringBytes("$-1\r\n"))
					continue
				}
				res := append(getStringBytes("+")[:], val.Value...)
				res = append(res[:], "\r\n"...)
				con.Write(res)
				
			default:
				con.Write(getStringBytes("-not implemented yet\r\n"))
			}
		}
	}
}

func getStringBytes(s string) []byte {
	return []byte(s)
}

func writeIfError(err error, con net.Conn) {
	if err != nil {
		con.Write([]byte(err.Error()))
	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
