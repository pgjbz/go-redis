package parser

import (
	"errors"
	"fmt"
	"strconv"

	"pgjbz.dev/go-redis/util"
)

type Command struct {
	Cmd   string
	Key   string
	Value []byte
	Args  map[string]string
}

func (c Command) String() string {
	return fmt.Sprintf("cmd = %v, key = %v, value = %v", c.Cmd, c.Key, c.Value)
}

func newCommand(cmd string, key string, value []byte, args map[string]string) Command {
	return Command{cmd, key, value, args}
}

func invalidInput(concat string) error {
	return errors.New("invalid input: " + concat)
}

type parser struct {
	bytes []byte
	idx   int
}

func (p *parser) consume() {
	p.idx++
}

func (p *parser) consumeSpecify(c byte) error {
	actual := p.bytes[p.idx]
	if actual != c {
		return invalidInput(fmt.Sprintf("expected %v, but got: %v\r\n", string(c), string(actual)))
	}
	p.consume()
	return nil
}

func (p *parser) skip(qtt int) {
	p.idx += qtt
}

func Parse(bytes []byte) ([]Command, error) {
	parser := &parser{
		bytes, 0,
	}

	commands := make([]Command, 0)
	if parser.bytes[0] != '*' {
		return nil, invalidInput("expected '*'")
	}

	parser.consume()
	if !util.IsNumber(bytes[parser.idx]) {
		return nil, invalidInput("expeted 'a number', got: " + string(bytes[parser.idx]))
	}
	parser.extractNumber()
	parser.skip(2)

	err := parser.consumeSpecify('$')
	if err != nil {
		return nil, err
	}

	if !util.IsNumber(bytes[parser.idx]) {
		return nil, invalidInput("expeted 'a number', got: " + string(bytes[parser.idx]))
	}

	cmd, err := parser.extractString()

	if err != nil {
		return nil, err
	}

	switch cmd {
	case "ping":
		commands = append(commands, newCommand(cmd, "", nil, nil))
		if util.IsAllZero(parser.bytes[parser.idx:]) {
			return commands, nil
		}

	case "get":
		err := parser.consumeSpecify('$')
		if err != nil {
			return nil, err
		}
		key, err := parser.extractString()

		if err != nil {
			return nil, err
		}
		commands = append(commands, newCommand(cmd, key, nil, nil))
	case "set":

		err := parser.consumeSpecify('$')
		if err != nil {
			return nil, err
		}

		key, err := parser.extractString()

		if err != nil {
			return nil, err
		}

		err = parser.consumeSpecify('$')
		if err != nil {
			return nil, err
		}

		value, err := parser.extractBytes()
		if err != nil {
			return nil, err
		}

		args := make(map[string]string)
		for !util.IsAllZero(parser.bytes[parser.idx:]) {
			err = parser.consumeSpecify('$')
			if err != nil {
				return nil, err
			}
			key, err := parser.extractString()
			if err != nil {
				return nil, err
			}
			err = parser.consumeSpecify('$')
			if err != nil {
				return nil, err
			}
			val, err := parser.extractString()
			if err != nil {
				return nil, err
			}
			args[key] = val
		}

		commands = append(commands, newCommand(cmd, key, value, args))

	default:
		commands = append(commands, newCommand("nsy", "", nil, nil))
	}

	return commands, nil
}

func (p *parser) extractBytes() ([]byte, error) {
	n := p.extractNumber()
	p.skip(2)
	sub := p.bytes[p.idx:(p.idx + n)]

	p.skip(n + 2)
	err := checkValidLenProvided(sub)
	if err != nil {
		return nil, err
	}

	return sub, nil
}

func (p *parser) extractString() (string, error) {
	bytes, err := p.extractBytes()
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func checkValidLenProvided(sub []byte) error {
	for _, v := range sub {
		if util.IsWhiteSpace(v) {
			return invalidInput("invalid len is provided")
		}
	}
	return nil
}

func (p *parser) extractNumber() int {
	start := p.idx
	for {
		p.consume()
		if !util.IsNumber(p.bytes[p.idx]) {
			break
		}
	}
	number, _ := strconv.ParseInt(string(p.bytes[start:p.idx]), 10, 64)
	return int(number)
}
