package database

import (
	"errors"
	"sync"
	"time"
)

type Data struct {
	Expire    bool
	Expire_at time.Time
	Value     []byte
}

type InMemory struct {
	rw     sync.RWMutex
	values map[string]Data
}

func NewData(value []byte) Data {
	return Data{
		Expire:    false,
		Expire_at: time.Now(),
		Value:     value,
	}
}

func (d *InMemory) Insert(key string, value Data) {
	d.rw.Lock()
	defer d.rw.Unlock()
	d.values[key] = value
}

func (d *InMemory) removeKey(key string) {
	d.rw.Lock()
	defer d.rw.Unlock()
	delete(d.values, key)
}

func (d *InMemory) Clear() {
	for k, v := range d.values {
		if v.Expire && time.Now().After(v.Expire_at) {
			d.removeKey(k)
		}
	}
}

func NewInMemory() InMemory {
	return InMemory{values: make(map[string]Data)}
}

func (d *InMemory) Get(key string) (*Data, error) {
	value, exists := d.values[key]
	if exists {
		return &value, nil
	}
	return nil, errors.New("value not found")
}
