package util

import "strconv"

func IsAllZero(bytes []byte) bool {
	for _, v := range bytes {
		if v != 0 {
			return false
		}
	}
	return true
}

func ParseInt(nstring string) int {
	val, _ := strconv.ParseInt(nstring, 10, 64)
	return int(val)
}

func IsWhiteSpace(b byte) bool {
	return b == ' ' || b == '\r' || b == '\n' || b == '\t' || b == '	'
}



func IsNumber(c byte) bool {
	return '0' <= c && c <= '9'
}
